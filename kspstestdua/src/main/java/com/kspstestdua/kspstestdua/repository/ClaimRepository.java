package com.kspstestdua.kspstestdua.repository;

import com.kspstestdua.kspstestdua.model.Claim;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ClaimRepository extends JpaRepository<Claim,Integer> {
   @Query("select c from Claim c")
    Page<List<Claim>> findAllClaims(Pageable pageable);

}
