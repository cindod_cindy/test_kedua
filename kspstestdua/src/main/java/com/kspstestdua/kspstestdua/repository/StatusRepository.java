package com.kspstestdua.kspstestdua.repository;

import com.kspstestdua.kspstestdua.model.Status;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

@Repository
public interface StatusRepository extends JpaRepository<Status,Integer> {
    Page<Status> findByClaimId(Integer claimId, Pageable pageable);

}
