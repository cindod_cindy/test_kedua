package com.kspstestdua.kspstestdua.repository;

import com.kspstestdua.kspstestdua.model.ClaimItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClaimItemRepository extends JpaRepository<ClaimItem,Integer>{
}
