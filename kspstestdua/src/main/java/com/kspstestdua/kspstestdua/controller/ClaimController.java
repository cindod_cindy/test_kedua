package com.kspstestdua.kspstestdua.controller;

import com.kspstestdua.kspstestdua.dto.request.RegisterRequest;
import com.kspstestdua.kspstestdua.dto.request.VerifyRequest;
import com.kspstestdua.kspstestdua.model.Claim;
import com.kspstestdua.kspstestdua.model.ClaimItem;
import com.kspstestdua.kspstestdua.model.Member;
import com.kspstestdua.kspstestdua.model.Status;
import com.kspstestdua.kspstestdua.service.ClaimService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/claim")
public class ClaimController {
//endpoint get all belum ke panggil swagger juga masih blm ke panggil
    @Autowired
    ClaimService claimService;

    @PostMapping("/register")
    public ResponseEntity<Object> createRegis(@RequestBody RegisterRequest registerRequest) {
               return claimService.createRegistrationClaim(registerRequest);
    }


//    //eror kalo d panggil
//    @GetMapping("/get-all-data")
//    public Optional<Claim> getAllClaimsData() {
//        return claimService.getAllClaims();
//    }

    @GetMapping("/get-all-data/claims")
    public Page<List<Claim>> getClaimData(Pageable pageable) {
        return claimService.getClaim(pageable);
    }

    @GetMapping("/get-all-data/claim-item")
    public List<ClaimItem> getClaimItemData() {
        return claimService.getItemClaim();
    }
    @GetMapping("/claims/{claimId}/status")
    public Page<Status> getAllStatusDataByClaimId(@PathVariable(value = "claimId") Integer claimId,
                                               Pageable pageable) {
        return claimService.getAllStatusByClaimId(claimId, pageable);
    }

    @PostMapping("/claims/{claimId}/status")
    public Status createStatus(@PathVariable (value = "claimId") Integer claimId,
                               @RequestBody Status status) {
        return claimService.createStatus(claimId,status);
           }

    @PutMapping("/verify/claims/{claimId}/status/{statusId}")
    public Status updateStatus(@PathVariable (value = "claimId") Integer claimId,
                               @PathVariable (value = "statusId") Integer statusId,
                               @RequestBody VerifyRequest verifyRequest) {
        return  claimService.updateStatus(claimId,statusId,verifyRequest);
           }

    @GetMapping("/claims/{claimId}/member")
    public Page<Member> getAllMemberByClaimId(@PathVariable(value = "claimId") Integer claimId,
                                               Pageable pageable) {
        return claimService.getAllMemberByClaimId(claimId, pageable);
    }

    @PostMapping("/claims/{claimId}/member")
    public Member createMember(@PathVariable (value = "claimId") Integer claimId,
                               @RequestBody Member member) {
        return claimService.createMember(claimId,member);
    }


}
