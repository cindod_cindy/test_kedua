package com.kspstestdua.kspstestdua;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@SpringBootApplication
@EnableJpaAuditing
@EnableWebMvc
public class KspstestduaApplication {

	public static void main(String[] args) {
		SpringApplication.run(KspstestduaApplication.class, args);
	}

}
