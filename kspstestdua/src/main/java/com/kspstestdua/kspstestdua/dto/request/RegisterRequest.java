package com.kspstestdua.kspstestdua.dto.request;

import com.kspstestdua.kspstestdua.model.ClaimItem;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;
import java.util.Date;
import java.util.Set;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class RegisterRequest {

    private String claimNumber;

    private String admissionDate;

    private String hospitalName;

    private String createdTime;

    private ClaimItem claimItem;

}
