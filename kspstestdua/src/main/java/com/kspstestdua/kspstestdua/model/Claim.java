package com.kspstestdua.kspstestdua.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "claim")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Claim extends AuditModel{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @Column(name = "id")
    private Integer id;

    @Column(name = "claim_number")
    private int claimNumber;

    @Column(name = "admission_date")
    private String admissionDate;

    @Column(name = "hospital_name")
    private String hospitalName;

    @Column(name = "created_time")
    private String createdTime;

    @OneToOne(targetEntity = ClaimItem.class, cascade = CascadeType.ALL)
    private ClaimItem claimItem;


}
