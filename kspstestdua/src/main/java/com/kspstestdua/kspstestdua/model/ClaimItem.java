package com.kspstestdua.kspstestdua.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "claim_item")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ClaimItem extends AuditModel{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "claim_item_id")
    private Integer claimItemId;

    @Column(name = "item_name")
    private String itemName;

    @Column(name = "value")
    private Double value;

    @OneToOne(targetEntity = Claim.class, mappedBy = "claimItem")
    private Claim claim;

}
