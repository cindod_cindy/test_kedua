package com.kspstestdua.kspstestdua.service;

import com.kspstestdua.kspstestdua.Exception.ResourceNotFoundException;
import com.kspstestdua.kspstestdua.dto.request.RegisterRequest;
import com.kspstestdua.kspstestdua.dto.request.VerifyRequest;
import com.kspstestdua.kspstestdua.model.Claim;
import com.kspstestdua.kspstestdua.model.ClaimItem;
import com.kspstestdua.kspstestdua.model.Member;
import com.kspstestdua.kspstestdua.model.Status;
import com.kspstestdua.kspstestdua.repository.ClaimItemRepository;
import com.kspstestdua.kspstestdua.repository.ClaimRepository;
import com.kspstestdua.kspstestdua.repository.MemberRepository;
import com.kspstestdua.kspstestdua.repository.StatusRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
public class ClaimService {


    @Autowired
    ClaimRepository claimRepository;

    @Autowired
    StatusRepository statusRepository;

    @Autowired
    MemberRepository memberRepository;

    @Autowired
    ClaimItemRepository claimItemRepository;


    @Transactional
    public ResponseEntity<Object> createRegistrationClaim(RegisterRequest registerRequest) {

        int forClaimNumber = Integer.parseInt(registerRequest.getClaimNumber());
        int insetClaimNumber = ++forClaimNumber;



        Claim claim = new Claim();
        claim.setClaimNumber(insetClaimNumber);
        claim.setAdmissionDate(registerRequest.getAdmissionDate());
        claim.setHospitalName(registerRequest.getHospitalName());
        claim.setCreatedTime(registerRequest.getCreatedTime());
        claim.setClaimItem(registerRequest.getClaimItem());
        Claim savedClaim = claimRepository.save(claim);
        if (claim== null){
            responseEntityError();
        }
        if(claimRepository.findById(savedClaim.getId()).isPresent())
        return ResponseEntity.ok().body("Claim Registered created successfully.");
        else return ResponseEntity.unprocessableEntity().body("Failed to create the claim registered specified.");
    }

//    //metod eror ketika di panggil
//    public Optional<Claim> getAllClaims() {
//        return claimRepository.findAllClaims();
//    }

    public Page<List<Claim>> getClaim(Pageable pageable) {
        return claimRepository.findAllClaims(pageable);
    }

    public List<ClaimItem> getItemClaim() {
        return claimItemRepository.findAll();
    }

    public ResponseEntity<String> responseEntityError(){
        return ResponseEntity.badRequest().body("Request Kosong");
    }


       public Page<Status> getAllStatusByClaimId(Integer claimId,
                                                  Pageable pageable) {
        return statusRepository.findByClaimId(claimId, pageable);
    }

      public Status createStatus( Integer claimId,
                                  Status status) {
        return claimRepository.findById(claimId).map(claim -> {
            status.setClaim(claim);
            return statusRepository.save(status);
        }).orElseThrow(() -> new ResourceNotFoundException("ClaimId " + claimId + " not found"));
    }


    public Status updateStatus(Integer claimId,
                               Integer statusId,
                               VerifyRequest verifyRequest) {
        if(!claimRepository.existsById(claimId)) {
            throw new ResourceNotFoundException("ClaimId " + claimId + " not found");
        }

        return statusRepository.findById(statusId).map(status -> {
            status.setStatus_type(verifyRequest.getStatus_type());
            return statusRepository.save(status);
        }).orElseThrow(() -> new ResourceNotFoundException("StatusId " + statusId + "not found"));
    }

    public Page<Member> getAllMemberByClaimId(Integer claimId,
                                              Pageable pageable) {
        return memberRepository.findByClaimId(claimId, pageable);
    }

    public Member createMember( Integer claimId,
                                Member member) {
        return claimRepository.findById(claimId).map(claim -> {
            member.setClaim(claim);
            return memberRepository.save(member);
        }).orElseThrow(() -> new ResourceNotFoundException("ClaimId " + claimId + " not found"));
    }

}
